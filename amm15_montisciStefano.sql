-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Gen 22, 2016 alle 18:29
-- Versione del server: 5.6.27-0ubuntu1
-- Versione PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amm15_montisciStefano`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `acquirente`
--

CREATE TABLE IF NOT EXISTS `acquirente` (
  `email` char(20) COLLATE utf8_bin NOT NULL,
  `marca` char(20) COLLATE utf8_bin NOT NULL,
  `modello` char(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `acquisti`
--

CREATE TABLE IF NOT EXISTS `acquisti` (
  `id` int(11) NOT NULL,
  `marca` char(20) COLLATE utf8_bin NOT NULL,
  `modello` char(20) COLLATE utf8_bin NOT NULL,
  `anno` char(20) COLLATE utf8_bin NOT NULL,
  `prezzo` char(20) COLLATE utf8_bin NOT NULL,
  `dataAcquisto` char(30) COLLATE utf8_bin NOT NULL,
  `emailVenditore` char(20) COLLATE utf8_bin NOT NULL,
  `emailAcquirente` char(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `motoInVendita`
--

CREATE TABLE IF NOT EXISTS `motoInVendita` (
  `marca` char(20) COLLATE utf8_bin NOT NULL,
  `modello` char(20) COLLATE utf8_bin NOT NULL,
  `prezzo` decimal(9,2) NOT NULL,
  `emailVenditore` char(20) COLLATE utf8_bin NOT NULL,
  `titolo` char(20) COLLATE utf8_bin NOT NULL,
  `anno` int(4) NOT NULL,
  `descrizione` char(200) COLLATE utf8_bin NOT NULL,
  `dataInserimento` char(30) COLLATE utf8_bin NOT NULL,
  `fotoPath` char(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `email` char(20) COLLATE utf8_bin NOT NULL,
  `password` char(20) COLLATE utf8_bin NOT NULL,
  `nome` char(20) COLLATE utf8_bin NOT NULL,
  `cognome` char(20) COLLATE utf8_bin NOT NULL,
  `ruolo` char(20) COLLATE utf8_bin NOT NULL,
  `indirizzo` char(30) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `persona`
--

INSERT INTO `persona` (`email`, `password`, `nome`, `cognome`, `ruolo`, `indirizzo`) VALUES
('', '', '', '', 'ruoloC', ''),
('acq@email.it', 'acq', 'acquirente', 'compro', 'ruoloC', 'via da li'),
('vend@email.it', 'vend', 'venditore', 'vendo', 'ruoloV', 'piazza la');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `acquirente`
--
ALTER TABLE `acquirente`
  ADD PRIMARY KEY (`email`,`marca`,`modello`);

--
-- Indici per le tabelle `acquisti`
--
ALTER TABLE `acquisti`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indici per le tabelle `motoInVendita`
--
ALTER TABLE `motoInVendita`
  ADD PRIMARY KEY (`marca`,`modello`);

--
-- Indici per le tabelle `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `acquisti`
--
ALTER TABLE `acquisti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
