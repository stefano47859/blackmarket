<?php /** progetto 1.0 **/
class Control{
	public function invoke(){ //legge i commandi passatagli
		$comando = isset($_REQUEST["comando"]) ? $_REQUEST["comando"] : "paginaIniziale"; // leggo il comando, per default "paginaIniziale"
		switch ($comando) { //ogni case dello switch prenderà il controllo delle pagine
		case "paginaIniziale": { //passo il controllo alla vista
			session_start(); //apro la sessione
			if(isset($_SESSION["userLogin"]) && $_SESSION["userLogin"]){ //se loggato
				$ruolo = $_SESSION["ruolo"]; //carico il tipo di ruolo
				switch ($ruolo) {
				case "ruoloC": include("view/paginaInizialeAcq.php"); break; //acquirente
				case "ruoloV": include("view/paginaInizialeVend.php"); break; //venditore
				}
			} else { //se disconnesso
				include("view/paginaInizialeDisc.php"); //disconnesso
			}
			break;
		} //end case paginaIniziale
		case "login": include("view/paginaLogin.php"); break;// passo il controllo alla vista "paginaAccesso.php"
		case "logout": include("model/sqlLogout.php"); break;// passo il controllo a "sqlLogout.php"
		case "registrati": include("view/paginaRegistrati.php"); break;// passo il controllo alla vista "paginaRegistrati.php"
		case "profilo": { // passo il controllo alla vista "paginaProfilo.php"
			include("model/sqlUtente.php");
			include("view/paginaProfilo.php");
			break;
		} //end case profilo
		case "inserisci": include("view/paginaInserisciAnnuncio.php"); break;// passo il controllo alla vista "paginaInserisciAnnuncio.php"
		case "annunci": { // passo il controllo alla vista "paginaAnnunci.php"
			include("view/paginaAnnunci.php");
			break;
		} //and case annunci
		case "dettagli": { // passo il controllo alla vista "paginaDettagli.php"
			include("view/paginaDettagli.php");
			break;
		} //and case dettagli
		case "acquisti": {
			include("model/sqlAcquisti.php");
			include("view/paginaAcquisti.php");
			break;
		} //and case acquisti
		} //end switch
	} //end invoke
} //end class Control
?>