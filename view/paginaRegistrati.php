<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaRegistrati.css" media="screen">
	<title>Registrazione</title>
</head>
<body>
<div id="container">
    <div id="sx">
        <a href="index.php"><div id="home"></div></a>
    </div>
	<div id="centr">
        <form action="model/sqlRegistra.php" method="post" name="formReg" onsubmit="return check();">
            <input type="email" id="email" name="email" class="log" placeholder="email"/>
            <input type="password" id="psw1" name="password1" class="log" placeholder="password1"/>
            <input type="password" id="psw2" name="password2" class="log" placeholder="password2"/>
            <input type="input" id="nome" class="log" name="nome" placeholder="nome"/>
            <input type="input" id="cognome" class="log" name="cognome" placeholder="cognome"/>
            <div class="log">
                <input type="radio" id="ruoloC" name="ruolo" value="ruoloC" checked>compratore
                <input type="radio" id="ruoloV" name="ruolo" value="ruoloV">venditore
            </div>
            <input type="input" id="address" class="log" name="indirizzo" placeholder="indirizzo"/>
            <div class="log">
                <input type="checkbox" name="contratto" value="si"/>
                <a href="contract.html" target="_blank">Accetta le condizioni</a>
            </div>
            <button type="submit" id="entra" class="button" value="Registrati">Registrati</button>
        </form>
	</div>
    <div id="dx"></div>
</div>
</body>
</html>
<script type="text/javascript">
function check()
{
	if(!(document.formReg.password1.value==document.formReg.password2.value)){alert("password diverse");return false;}
	if(!(document.formReg.contratto.checked)){alert("accetta le condizioni");return false;}
	return true;
}
</script>