<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaProfilo.css" media="screen">
	<title>Profilo</title>
</head>
<body>
<div id="container">
	<div id="sx">
		<a href="index.php"><div id="home"></div></a>
	</div>
	<div id="centr">
		<div id="titolo">PANNELLO UTENTE</div>
		<div id="infoCentr">
            <div class="lbl">Email: <?php print $data['email'];?></div>
            <div class="lbl">Password: <?php print $data['password'];?></div>
            <div class="lbl">Nome: <?php print $data['nome'];?></div>
            <div class="lbl">Cognome: <?php print $data['cognome'];?></div>
            <div class="lbl">Ruolo: <?php print $data['ruolo'];?></div>
            <div class="lbl">Indirizzo: <?php print $data['indirizzo'];?></div>
		</div>
	</div>
	<div id="dx">
    	<a href="index.php?comando=acquisti"><div id="acquisti">ACQUISTI</div></a>
	</div>
</div>
</body>
</html>