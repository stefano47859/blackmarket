<!--/** progetto 1.0 **/-->
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaIniziale.css" media="screen">
	<link rel="stylesheet" type="text/css" href="style/animate.css" media="screen">
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="script/paginaIniziale.js"></script>
	<title>Black Market</title>
</head>
<body>
<div id="container">
	<div id="contSx">
	</div>
	<div id="central">
    	<div id="cart"></div>
		<img id="photo" src="img/scorrimento/00.jpg"></img>
	</div>
	<div id="contDx">
		<a href="?comando=registrati"><div class="menuBtD">REGISTRATI</div></a>
        <a href="?comando=login"><div class="menuBtD">LOGIN</div></a>
	</div>
</div>
</body>
</html>