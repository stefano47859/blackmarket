<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaDettagli.css" media="screen">
	<title>Dettagli</title>
</head>
<body>
<div id="container">
	<div id="sx">
    	<a href="index.php"><div id="home"></div></a>
    </div>
    <div id="centr">
    	<form action="model/sqlCompra.php" method="post" name="formCompra">
            <div id="titInfo">
            	<input type="input" id="titolo" class="text" name="titolo" value="<?php echo $_POST["titolo"]; ?>" readonly/>
                <div id="info">
                	<input type="input" id="marca" class="text" name="marca" value="<?php echo $_POST["marca"]; ?>" readonly/>
                    <input type="input" id="modello" class="text" name="modello" value="<?php echo $_POST["modello"]; ?>" readonly/>
                    <input type="input" id="anno" class="text" name="anno" value="<?php echo $_POST["anno"]; ?>" readonly/>
                    <input type="input" id="prezzo" class="text" name="prezzo" value="<?php echo $_POST["prezzo"]; ?>" readonly/>
                </div>
            </div>
            <button type="submit" id="compra" class="text" value="Compra">Compra</button>
            <div id="foto">
                <div id="image"></div>
                <div id="prev">PREV</div>
                <div id="next">NEXT</div>
            </div>
            <textarea id="descrizione" class="text" name="descrizione" maxlength="200" readonly><?php echo $_POST["descrizione"]; ?></textarea>
        </form>
    </div>
    <div id="dx"></div>
</div>
</body>
</html>