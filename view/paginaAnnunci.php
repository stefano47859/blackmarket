<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaAnnunci.css" media="screen">
	<title>Annunci</title>
</head>
<body>
<div id="container">
	<div id="sx">
    	<a href="index.php"><div id="home"></div></a>
    </div>
    <div id="centr">
        <div id="titolo">ANNUNCI TROVATI</div>
        <ul id="annunci">
			<?php
				$orig = unserialize($_GET["data"]); //decomprimo i dati della tabella motoInVendita
				for ($x = 0; $x < $_GET["numRig"]; $x++) { //cicla per un numero uguale alla quantità di annunci trovati
			?>
                <li>
                    <form action="index.php?comando=dettagli" method="post" name="formDettagli">
                        <img id="foto" src="<?php echo $orig[$x]["fotoPath"]; ?>" alt="fotoAnnuncio" style="width:86px;height:86px;background-size:cover">
                        <div id="info">
                        	<button type="submit" id="infoTit" name="titolo" value="<?php echo $orig[$x]["titolo"]; ?>"><?php echo $orig[$x]["titolo"]; ?></button>
                            <input type="input" id="data" name="data" value="<?php echo $orig[$x]["dataInserimento"];?>"/>
                            <span id="item">
                            	<input type="input" id="marca" name="marca" value="<?php echo $orig[$x]["marca"];?>"/>
                                <input type="input" id="modello" name="modello" value="<?php echo $orig[$x]["modello"];?>"/>
                                <input type="input" id="anno" name="anno" value="<?php echo $orig[$x]["anno"];?>"/>
                            </span>
                        </div>
                        <input type="input" id="prezzo" name="prezzo" value="<?php echo $orig[$x]["prezzo"];?> €"/>
                        <input type="hidden" name="descrizione" id="descrizione" value="<?php echo $orig[$x]["descrizione"]; ?>" />
                    </form>
                </li>
            <?php 
				} //end for x 
			?>
        </ul>
    </div>
    <div id="dx"></div>
</div>
</body>
</html>