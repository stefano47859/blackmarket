<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaInserisciAnnuncio.css" media="screen">
	<title>Inserisci Annuncio</title>
</head>
<body>
<div id="container">
	<div id="sx">
    	<a href="index.php"><div id="home"></div></a>
    </div>
    <div id="centr">
        <form action="model/sqlInserisciAnnuncio.php" enctype="multipart/form-data" method="post" name="formInsAnn">
            <input type="input" id="titolo" class="ins" name="titolo" placeholder="Inserisci qui il titolo dell'annuncio"/>
            <input type="input" id="marca" class="ins2" name="marca" placeholder="Marca"/>
            <input type="input" id="modello" class="ins2" name="modello" placeholder="Modello"/>
            <input type="input" id="anno" class="ins2" name="anno" placeholder="Anno" maxlength="4"/>
            <textarea id="descrizione" class="ins" name="descrizione" placeholder="Inserisci qui una descrizione per il prodotto" maxlength="200"></textarea>
            <input type="input" id="prezzo" class="ins" name="prezzo" placeholder="Prezzo"/>
            <input type="file" id="f1" name="image"/>
            <button type="submit" id="inserisci" value="Inserisci">Inserisci</button>
        </form>
    </div>
    <div id="dx"></div>
</div>
</body>
</html>