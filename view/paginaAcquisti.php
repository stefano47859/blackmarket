<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Stefano Montisci">
	<meta name="description" content="Progetto per il corso di AMM">
	<link rel="stylesheet" type="text/css" href="style/paginaAcquisti.css" media="screen">
	<title>Profilo</title>
</head>
<body>
<div id="container">
	<div id="sx">
		<a href="index.php"><div id="home"></div></a>
	</div>
	<div id="centr">
		<div id="titolo">ACQUISTI</div>
		<div id="infoCentr">
        	<?php
				$data = unserialize($serial); //decomprimo i dati della tabella acquisti
				for ($x = 0; $x < $numRig; $x++) { //cicla per un numero uguale alla quantità di annunci trovati
			?>
                <li id="elem">
                    <div id="image"></div>
                    <div id="info">
                    <div class="lbl">Marca: <?php print $data[$x]["marca"];?></div>
                    <div class="lbl">Modello: <?php print $data[$x]["modello"];?></div>
                    <div class="lbl">Anno: <?php print $data[$x]["anno"];?></div>
                    <div class="lbl">Prezzo: <?php print $data[$x]["prezzo"];?></div>
                    <div class="lbl">Data di acquisto: <?php print $data[$x]["dataAcquisto"];?></div>
                    <div class="lbl">Venditore: <?php print $data[$x]["emailVenditore"];?></div>
                    </div>
                </li>
            <?php 
				} //end for x 
			?>
		</div>
	</div>
	<div id="dx"></div>
</div>
</body>
</html>